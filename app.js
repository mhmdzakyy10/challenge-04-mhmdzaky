const express = require("express");
const app = express();
const { UserGame, UserGameBiodata, UserGameHistory } = require("./models");
const port = process.env.PORT || 4000;

app.use(express.json());

//GET ALL
// get all UserGame
app.get("/user_game", (req, res) => {
  UserGame.findAll()
    .then((user_game) => {
      res.status(200).json({ message: "Success", data: user_game });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error", err });
    });
});

// get all UserGameBiodata
app.get("/user_game_biodata", (req, res) => {
  UserGameBiodata.findAll()
    .then((user_game_biodata) => {
      res.status(200).json({ message: "Success", data: user_game_biodata });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error", err });
    });
});

// get all UserGameHistory
app.get("/user_game_history", (req, res) => {
  UserGameHistory.findAll()
    .then((user_game_history) => {
      res.status(200).json({ message: "Success", data: user_game_history });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error", err });
    });
});

// GET BY ID
//user game by id
app.get("/user_game/:id", (req, res) => {
  UserGame.findOne({
    where: { id: req.params.id },
  })
    .then((user_game) => {
      res.status(200).json({ message: "Success", data: user_game });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error", err });
    });
});

// user game biodata by id
app.get("/user_game_biodata/:id", (req, res) => {
  UserGameBiodata.findOne({
    where: { id: req.params.id },
  })
    .then((user_game_biodata) => {
      res.status(200).json({ message: "Success", data: user_game_biodata });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error", err });
    });
});

// user game history by id

app.get("/user_game_history/:id", (req, res) => {
  UserGameHistory.findOne({
    where: { id: req.params.id },
  })
    .then((user_game_history) => {
      res.status(200).json({ message: "Success", data: user_game_history });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error", err });
    });
});

// POST USER GAME
app.post("/user_game", (req, res) => {
  UserGame.create({
    username: req.body.username,
    password: req.body.password,
  })
    .then((user_game) => {
      res.status(200).json({ message: "Success", data: user_game });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error creating user game", err });
    });
});

// POST USER GAME BIODATA
app.post("/user_game_biodata", (req, res) => {
  UserGameBiodata.create({
    nama_user: req.body.nama_user,
    jenis_kelamin: req.body.jenis_kelamin,
    umur: req.body.umur,
    email: req.body.email,
    id_user: req.body.id_user,
  })
    .then((user_game_biodata) => {
      res.status(200).json({ message: "Success", data: user_game_biodata });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error creating user game", err });
    });
});

// POST USER GAME HISTORY
app.post("/user_game_history", (req, res) => {
  UserGameHistory.create({
    nama_game: req.body.nama_game,
    waktu_bermain: req.body.waktu_bermain,
    skor_game: req.body.skor_game,
    id_user: req.body.id_user,
  })
    .then((user_game_history) => {
      res.status(200).json({ message: "Success", data: user_game_history });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error creating user game", err });
    });
});

// UPDATE USER GAME
app.put("/user_game/update/:id", (req, res) => {
  const { username, password } = req.body;
  UserGame.update({ username, password }, { where: { id: req.params.id } })
    .then((user_game) => {
      res.status(200).json({ message: "User Game Updated", data: user_game });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error updating user game", err });
    });
});

// UPDATE USER GAME BIODATA
app.put("/user_game_biodata/update/:id", (req, res) => {
  const { nama_user, jenis_kelamin, umur, email, id_user } = req.body;
  UserGameBiodata.update({ nama_user, jenis_kelamin, umur, email, id_user }, { where: { id: req.params.id } })
    .then((user_game_biodata) => {
      res.status(200).json({ message: "User Game Biodata Updated", data: user_game_biodata });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error updating user game", err });
    });
});

// UPDATE USER GAME HISTORY
app.put("/user_game_history/update/:id", (req, res) => {
  const { nama_game, waktu_bermain, skor_game, id_user } = req.body;
  UserGameHistory.update({ nama_game, waktu_bermain, skor_game, id_user }, { where: { id: req.params.id } })
    .then((user_game_history) => {
      res.status(200).json({ message: "User Game History Updated", data: user_game_history });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error updating user game", err });
    });
});

// DELETE USER GAME
app.delete("/user_game/:id", (req, res) => {
  UserGame.destroy({ where: { id: req.params.id } })
    .then((user_game) => {
      res.status(200).json({ message: "User Game Deleted", data: user_game });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error deleting user game", err });
    });
});

// DELETE USER GAME BIODATA
app.delete("/user_game_biodata/:id", (req, res) => {
  UserGameBiodata.destroy({ where: { id: req.params.id } })
    .then((user_game_biodata) => {
      res.status(200).json({ message: "User Game Biodata Deleted", data: user_game_biodata });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error deleting user game", err });
    });
});

// DELETE USER GAME HISTORY
app.delete("/user_game_history/:id", (req, res) => {
  UserGameHistory.destroy({ where: { id: req.params.id } })
    .then((user_game_history) => {
      res.status(200).json({ message: "User Game History Deleted", data: user_game_history });
    })
    .catch((err) => {
      res.status(422).json({ message: "Error deleting user game", err });
    });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
